import json

###################### Identity ######################

# ID
identifiant = ""
while identifiant.isdigit() == False: 
    identifiant = input("Entrer votre ID ")

# FirstName
firstName = input("Entrer votre prénom ")

# LastName
lastName = input("Entrer votre nom ")

# Birthday
birthDay = ""
birthValidation = False
while birthValidation == False:
    birthDay = input("Entrer votre date de naissance au format JJ/MM/AAAA ")
    if len(birthDay) == 10:
        if birthDay[0:1].isdigit():
            if birthDay[3:4].isdigit():
                if birthDay[6:9].isdigit():
                    if birthDay[2] == "/":
                        if birthDay[5] == "/":
                            birthValidation = True

# Mail
mail = input("Entrer votre mail ")

# Phone
phone  = ""
while phone.isdigit() == False:
    phone = input("Entrer votre numéro de téléphone ")

# Picture
image = "profil.png"


###################### Social Network ##########################
fb = input("Entrer votre lien fb ")
lkdn = input("Entrer votre lien LinkedIn ")
twtr = input("Entrer votre lien Twitter ")
insta = input("Entrer votre lien Instagram ")
github = input("Entrer votre lien Github ")
gitlab = input("Entrer votre lien Gitlab ")
so = input("Entrer votre lien StackOverflow ")

# Other network
reseaux = []
autreReseau = ""
while autreReseau != "N":
    autreReseau = input("Avez vous d'autres réseau à ajouter? (O/N)")
    if autreReseau == "O":
        socials = {}
        nomReseau = input("Enter le nom du réseau ")
        lienReseau = input("Entrer le lien du réseau ")
        socials["name"]=nomReseau
        socials["link"]=lienReseau
        reseaux.append(socials)

# CV
pdfCV = "cv.pdf"
linkCV = input("Entrer le lien de votre CV ")

# Realisations
realisations = []
autreRealisation = ""
while autreRealisation != "N":
    autreRealisation = input("Avez vous d'autres realisations à ajouter? (O/N) ")
    if autreRealisation == "O":
        achievements = {}
        nomRealisation = input("Enter le nom de la realisation ")
        snapshot = nomRealisation + ".png"
        lienRealisation = input("Entrer le lien de la realisation ")
        desc = input("Entrer une description de votre réalisation ")
        achievements["snapshot"] = snapshot
        achievements["link"] = lienRealisation
        achievements["name"] = nomRealisation
        achievements["desc"] = desc
        realisations.append(achievements)

# Video
video = "video.mp4"

# Badges
badges = []
autreBadge = ""
while autreBadge != "N":
    autreBadge = input("Avez vous d'autres badges à ajouter? (O/N) ")
    if autreBadge == "O":
        dictionnary = {}
        idBadge = input("Entrer l'Id du badge ")

        enabled = ""
        while enabled != "O" and enabled != "N":
            enabled = input("Avez vous déjà ce badge? (O/N) ")
        boolean = False
        if enabled == "O":
            boolean = True
        dictionnary["id"] = int(idBadge)
        dictionnary["enabled"] = boolean
        badges.append(dictionnary)

# Status
status = ""
while status != "O" and status != "N":
    status = input("Avez vous trouvé votre alternance? (O/N) ")
adopted = False
if status == "O":
    adopted = True

# Promo
promo = "A2 Dev"

# :TODO How use comments ? Back-end ? Fake ?
comments = []


#################### Create JSON ########################## 
convert = {
    "id": int(identifiant),
    "contact": {
        "firstName": firstName,
        "lastName": lastName,
        "birthday": birthDay,
        "mail": mail,
        "phone": phone,
        "img": image
    },
    "social": {
        "fb": fb,
        "lkdn": lkdn,
        "twtr": twtr,
        "insta": insta,
        "github": github,
        "gitlab": gitlab,
        "so": so,
        "Others": reseaux
    },
    "medias": {
        "pdfCV": pdfCV,
        "linkCV": linkCV,
        "realisations": realisations,
        "video": video
    },
    "badges": badges,
    "status": {
        "adopted": adopted,
        "promo": promo,
    },
    "comments": comments
}

fileName = identifiant + ".json"

with open(fileName, 'w') as outfile:
    json.dump(convert, outfile)