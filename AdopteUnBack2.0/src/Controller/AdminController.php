<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Badge;
use App\Entity\Comment;
use App\Entity\User;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_homepage")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Comment::class)->findBy(['validated'=>null]);
        $users = $em->getRepository(User::class)->findAll();
        $badges = $em->getRepository(Badge::class)->findAll();
        $formattedArray = [];
        foreach ($badges as $badge){
            array_push($formattedArray, [
                'id' => $badge->getId(),
                'name' => $badge->getName(),
                'img' => 'asset/badges/'.$badge->getImg()
            ]);
        }

        return $this->render('admin/dashboard.html.twig', [
            'badges' => $formattedArray,
            'comments' => $comments,
            'users' => $users,
        ]);
    }
}
