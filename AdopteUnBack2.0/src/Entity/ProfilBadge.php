<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfilBadgeRepository")
 */
class ProfilBadge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Badge", inversedBy="profilBadges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $badgeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Profil", inversedBy="profilBadges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profilId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBadgeId(): ?Badge
    {
        return $this->badgeId;
    }

    public function setBadgeId(?Badge $badgeId): self
    {
        $this->badgeId = $badgeId;

        return $this;
    }

    public function getProfilId(): ?Profil
    {
        return $this->profilId;
    }

    public function setProfilId(?Profil $profilId): self
    {
        $this->profilId = $profilId;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }
}
